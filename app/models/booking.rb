class Booking < ActiveRecord::Base
  belongs_to :guest, class_name: "User", foreign_key: "user_id"
  belongs_to :room 
  belongs_to :hotel
end
