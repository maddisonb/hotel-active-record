class User < ActiveRecord::Base
  has_many :bookings
  has_many :booked_rooms, through: :bookings, source: :room
  has_many :hotels, through: :bookings
end
