class Room < ActiveRecord::Base
  has_many :bookings
  has_many :users, through: :bookings
  has_one :hotel, through: :bookings
end
