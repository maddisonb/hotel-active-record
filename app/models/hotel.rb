class Hotel < ActiveRecord::Base
  has_many :bookings
  has_many :rooms, through: :bookings
  has_many :booked_guests, through: :bookings, source: :guest
end
