class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.belongs_to :user
      t.belongs_to :room
      t.belongs_to :hotel
    end
  end
end
